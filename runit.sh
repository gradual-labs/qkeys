

f=OTP.dot
perl OTP.pl
dot ${f%.*}.dot -Tpng -o ${f%.*}_dot.png
neato ${f%.*}.dot -Tpng -o ${f%.*}_net.png
fdp ${f%.*}.dot -Tpng -o ${f%.*}_fdp.png
twopi ${f%.*}.dot -Tpng -o ${f%.*}_2pi.png
circo ${f%.*}.dot -Tpng -o ${f%.*}_cir.png
patchwork ${f%.*}.dot -Tpng -o ${f%.*}_pat.png
osage ${f%.*}.dot -Tpng -o ${f%.*}_osa.png

