#!/usr/bin/perl
# $Id: OTP.pl,v 0.8.9 2020-6-08 18:08:38 michelc Exp $

printf "--- # %s \n",$0;
our $exp = &is_experimental();

# dual polarity OTP
require "seed.sec"
my $ab = 'AGTC'; # 4 codes
my $ab = '01234567AGTC'; # 12 codes
my $ab = '-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+';
my $alphab = &randomize($ab.  q/:.@*$%_,~`'=;!?^[]{}()<>#&/.'"/| \\');

printf "seed: %s\n",$seed;
printf "ab: %s\n",$ab;
printf "αβet: %s\n",$alphab;


# stem :
my $seedm = shake(160,$seed);
my $seedk = shake(160,$secret);
printf "seedm: %s\n",&encode_basea($seedm,$alphab);
printf "seedk: %s\n",&encode_basea($seedk,$alphab);

my $mask = &encode_basea(pack('N',int rand(1<<32)),$alphab);
printf "mask: %s\n",$mask;

my $n = 5;
# ------------------------------------------------
# chain 1: # (mind-matter) syntropic
my @skm,@keysm;
my $ski = shake(160,$seedm);
print "skm: |-\n";
for (0 .. $n+1) {
   push @skm,$ski;
   my $skia = &encode_basea($ski,$ab);
   push @keysm,$skia;
   printf " %s\n",$skia;
   $ski = shake(160,$ski); # next one
}
push @skm,$ski;
my $skia = &encode_basea($ski,$ab);
push @keysm,$skia;
# ------------------------------------------------
# chain 2: # (anti-matter) entropic
my @skk,@keysk;
my $ski = shake(160,$seedk);
print "skk: |-\n";
for (0 .. $n+1) {
   push @skk,$ski;
   my $skia = &encode_basea($ski,$ab);
   push @keysk,$skia;
   printf " %s\n",$skia;
   $ski = shake(160,$ski); # next one...
}
push @skk,$ski;
my $skia = &encode_basea($ski,$ab);
push @keysk,$skia;
# ------------------------------------------------
# create dot file :
#
local *DOT; open DOT,'>','OTP.dot';
print DOT "digraph {\n";
print DOT "rankdir=LR;\n";
#print DOT " partikis -> { seedm seedk } [constraint=false]\n";
printf DOT qq'{ rank=same; "seedm" "seedk" "otp"; }';

# ---------------
print DOT qq' subgraph "pkm-seed" { \n';
print DOT qq'  label="partikum;"\n';
print DOT qq'  color=red;\n';
my $prev = 'seedm';
for (0 .. $n+1) {
   my $skia = $keysm[$_];
   my $cur = substr($skia,-4);
   printf DOT qq'"%s" [label="%s t=%s"]\n',$cur,$cur,$_;
   printf DOT qq'"%s" -> "%s" [taillabel=%u constraint=false]\n',$prev,$cur,$_;
   $prev = $cur;
}
#my $last = substr($keysm[-1],-4);
#printf DOT qq'"%s" -> "%s"\n',$prev,$last;
print DOT " } \n";
# ---------------
print DOT qq' subgraph pka { \n';
print DOT qq'  label="partika;"\n';
print DOT qq'  color=blue;\n';
my $prev = 'seedk';
for (0 .. $n+1) {
   my $cur = substr($keysk[$_],-4);
   printf DOT qq'"%s" [label="%s t=%s"]\n',$cur,$cur,$n-$_;
   printf DOT qq'"%s" -> "%s" [taillabel=%u constraint=false]\n',$prev,$cur,$_;
   $prev = $cur;
}
#my $last = substr($keysk[-1],-4);
#printf DOT qq'"%s" -> "%s"\n',$prev,$last;
print DOT " } \n";
# ---------------


# mixing :
my @otp;
my $prev = 'otp';
print DOT qq' subgraph "OTP-keys" { \n';
print DOT qq'  label= "OTP keys;"\n';
print DOT qq'  style=filled;\n';
print DOT qq'  color=lightgrey;\n';

printf DOT qq'"%s"\n',$prev;
for (0 .. $n+1) {
   my $otpi = shake(160,$skk[0+$_],$skm[$n-$_]);
   my $otpia = &encode_basea($otpi,$ab);
   printf " %s\n",$otpia;
   my $cur = substr($otpia,-4);
   printf "%u: %s,%s -> %s\n",$_,$fa,$mo,$cur;
    printf DOT qq'"%s" [shape="box"]\n',$cur;
    printf DOT qq'"%s" -> "%s" [style="dashed"]\n',$prev,$cur;
   $prev = $cur;
   push @otp,$otpi;
}
print DOT " } \n";

for (0 .. $n) {
   my $otpi = shake(160,$skk[0+$_],$skm[$n-$_]);
   my $otpia = &encode_basea($otpi,$ab);
   my $cur = substr($otpia,-4);
   my $fa = substr(&encode_basea($skm[$n-$_],$ab),-4);
   my $mo = substr(&encode_basea($skk[$_],$ab),-4);
    printf DOT qq'"%s" -> "%s" [label="%d-%d"]\n',$fa,$cur,$n,$_;
    printf DOT qq'"%s" -> "%s" [headlabel=%u]\n',$mo,$cur,$_;
    printf DOT qq'{ rank=same; "%s" "%s" }\n',$fa,$mo;
}

    #printf DOT qq'{ rank=source; "%s" -> "%s"}\n',$fa,$cur;
    #printf DOT qq'{ rank=source; "%s" -> "%s"}\n',$mo,$cur;
print DOT "}\n";
close DOT;



# ---------------------------
sub xor {
 my @a = unpack'Q*',$_[0] . "\0"x7;
 my @b = unpack'Q*',$_[1] . "\0"x7;
 my @x = ();
 foreach my $i (0 .. $#a) {
   $x[$i] = $a[$i] ^ $b[$i];
   printf "%08X = %08X ^ %08X\n",$x[$i],$a[$i],$b[$i] if $dbug;
 }
 my $x = pack'Q*',@x;
}
# ---------------------------
sub randomize {
  my $a = shift;
  my @b = sort { rand(1)<0.5 ? -1 : 1 } (split'',$a);
  return join'',@b;
}
# ---------------------------
sub encode_basea { # w. a passed alphabet
  use Math::BigInt;
  my ($data,$alphab) = @_;
  $alphab = '123456789ABCDEFGHJKLMNPQRSTUVWXYZ -+.$%' unless $alphab; # barcode 3 to 9
  my $radix = length($alphab);
  my $mod = Math::BigInt->new($radix);
  #printf "mod: %s, lastc: %s\n",$mod,substr($alphab,$mod,1);
  my $h = '0x'.unpack('H*',$data);
  my $n = Math::BigInt->from_hex($h);
  my $e = '';
  while ($n->bcmp(0) == +1)  {
    my $c = Math::BigInt->new();
    ($n,$c) = $n->bdiv($mod);
    $e .= substr($alphab,$c->numify,1);
  }
  return scalar reverse $e;
}
# -----------------------------------------------------
sub shake { # use shake 128
  use Crypt::Digest::SHAKE;
  my $len = shift;
  my $x = ($len >= 160) ? 256 : 128;
  my $msg = Crypt::Digest::SHAKE->new($x);
  $msg->add(join'',@_);
  my $digest = $msg->done(($len+7)/8);
  return $digest;
}
# -----------------------------------------------------
sub get_shake { # use shake 256 because of ipfs' minimal length of 20Bytes
  use Crypt::Digest::SHAKE;
  my $len = shift;
  local *F; open F,$_[0] or do { warn qq{"$_[0]": $!}; return undef };
  #binmode F unless $_[0] =~ m/\.txt/;
  my $msg = Crypt::Digest::SHAKE->new(256);
  $msg->addfile(*F);
  my $digest = $msg->done(($len+7)/8);
  return $digest;
}
# -----------------------------------------------------
sub is_experimental {
  my $p = tell DATA;
  seek(DATA,0,0); local $/ = "\n";
  my $line = <DATA>;
     $line = <DATA>;
  seek(DATA,$p,0);
  #printf "line%u: %s",$.,$line;
  my $rcsid = "$1" if ($line =~ m/\$Id\:\s*(.*?)\s*\$/);
  #printf "rcsid: '%s'\n",$rcsid;
  my $exp = ($rcsid =~ m/Exp$/) ? 1 : 0;
  #printf "exp: %s\n",$exp;
  return $exp;
}

1; # $Source: /my/perl/scripts/OTP.pl $
__DATA__
